# Summary of Scripts
These scripts will be triggered automantically when there is a new database version detected. They will prepare the raw db files (e.g. index them) so the searching software can be run (e.g. hmmscan, tigrfam, & lastal).

These scripts are using docker image as so:
`jfroula/prepare_db:1.0.1 preprocess_pfam.py`

**preprocess_pfam.py**
This script uses hmmpress to create indexed files required for hmmscan

**preprocess_rfam.py**
This script uses hmmpress to create indexed files required for cmsearch

**preprocess_tigrfam.py**
This script uses hmmpress to create indexed files required for tigrfam (and maybe interproscan)

**preprocess_uniparc.py**
This script doesn't do anything at this point.

**preprocess_suprfam.py**
This script doesn't do anything. The database will be manually updated.
If I ever turn it on, it should use the following commands to prepare db.

```
gunzip pdbj95d.gz
gunzip model.tab.gz
gunzip hmmlib_1.75.gz
mv hmmlib_1.75 hmmlib
jaws gunzip self_hits.tab.gz
mkdir scratch
chmod u + x *.pl
hmmpress hmmlib
```

**preprocess_uniref50.py**
This script uses lastdb to create indexed files required for lastal

**preprocess_uniref90.py**
This script uses lastdb to create indexed files required for lastal

**preprocess_uniref100.py**
This script uses lastdb to create indexed files required for lastal
