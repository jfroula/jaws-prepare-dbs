FROM continuumio/miniconda

RUN conda install -y -c bioconda hmmer==3.2.1
RUN conda install -y -c bioconda infernal==1.1.2
RUN conda install -y -c bioconda last=941 

COPY bin/ /usr/local/bin
