#!/usr/bin/env python
import pika

# make connection
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# create a queue (don't know if already created by send.py)
channel.queue_declare(queue='hello')

# print message to screen
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

# messages given to callback should come from hello queue
channel.basic_consume(queue='hello',
                      auto_ack=True,
                      on_message_callback=callback)

# loop waits for message
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()


