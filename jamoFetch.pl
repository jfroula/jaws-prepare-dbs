#!/usr/bin/env perl

use strict;
use File::Basename;
use constant { "FILES_PER_REQUEST" => 100, SLEEP_DUR1 => 1, SLEEP_DUR2 => 60 };

@ARGV or die("Usage: jamoFetch.pl <days> < jamo_ids.txt\n");
my $days = shift @ARGV;
$days > 0 or die("Invalid number of days: $days\n");

# READ IDS AND FETCH
my @ids;
my %ids;
my $id;
my $output;
my $status;
my $key;
while (<STDIN>)
{
	chomp;
	($id) = split(/\t/);
	push @ids, $id;
	$ids{$id} = undef;
	if (@ids == FILES_PER_REQUEST )
	{
		print `jamo fetch -d $days id @ids`;
		@ids = ();
		sleep(SLEEP_DUR1);
	}
}
@ids and print `jamo fetch -d $days id @ids`;

# GET FILE PATHS AND CHECK IF EXISTS
print "Blocking until all files are available\n";
my ($dir, $filename, $file);
foreach $id ( keys %ids )
{
	sleep(SLEEP_DUR1);
	$output = `jamo report select file_path, file_name where _id = $id`;
	chomp $output;
	my ($dir, $filename) = split(/\s+/, $output);
	$file = $ids{$id} = "$dir/$filename";
	if ( -e $file ) { delete($ids{$id}) }
}

# BLOCK UNTIL AVAILABLE
do {
	sleep(scalar(keys %ids) * SLEEP_DUR2 );
	foreach $id ( keys %ids )
	{
		$file = $ids{$id};
		if ( -e $file ) { delete($ids{$id}) }
		sleep(SLEEP_DUR1);
	}
} while ( scalar(keys %ids) );

