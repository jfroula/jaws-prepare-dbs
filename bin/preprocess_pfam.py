#!/usr/bin/env python
import sys
import os
import argparse
import subprocess

file_compressed = 'Pfam-A.hmm.gz'
file_uncompressed = 'Pfam-A.hmm'
release_notes = 'relnotes.txt'

if (len(sys.argv) < 2):
    print("Usage: {} -s <source dir> -o <outdir>".format(sys.argv[0]))
    sys.exit()

# parse arguments
fasta=''
parser = argparse.ArgumentParser(description='Takes a directory of reference files and creates indexed database files in a new versioned folder.')
parser.add_argument("-s", "--source",help="A directory path where the un-indexed reference files are kept", type=str)
parser.add_argument("-o","--outdir",help="The outdir is where a versioned folder will be saved as well as a link to it representing the latest database version.", type=str)
args = parser.parse_args()


try:
    dbfile = [os.path.join(args.source,f) for f in os.listdir(args.source) if f.endswith(file_compressed)][0]
except:
    print("File not found: {}".format(file_compressed))
    sys.exit(1)

if (not dbfile.endswith("*.gz")):
    print("database file found {}".format(dbfile))
else:
    print("This file {} doesn't seem to have a gz suffix.  We were expecting {}".format(dbfile, file_compressed))
    sys.exit()

try:
    relnotes = [os.path.join(args.source,f) for f in os.listdir(args.source) if f.endswith(release_notes)][0]
except:
    print("File not found: {}".format(release_notes))
    sys.exit(1)

dbfile = os.path.abspath(dbfile)
relnotes = os.path.abspath(relnotes)


# find the release version
cmd='head -2 '+ relnotes + '| tail -1'
process = subprocess.run([cmd], shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
output = process.stdout
version = output.strip().split()[1]
print("## version " + version)

# we can now make a versioned directory in the output dir
save_dir = os.path.join(args.outdir,version)
os.makedirs(save_dir, exist_ok=True)
print("## created {}".format(save_dir))

# process the raw files to create indexed references
out_db_file = os.path.join(save_dir,file_uncompressed)

cmd='zcat ' + dbfile + ' > ' + out_db_file
print("## " + cmd)
process = subprocess.run([cmd], shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
output = process.stdout
print("## " + output)

os.chdir(save_dir)
cmd='hmmpress ' + file_uncompressed
print("## " + cmd)
process = subprocess.run([cmd], shell=True, check=True, stdout=subprocess.PIPE, universal_newlines=True)
output = process.stdout
print("## " + output)

# make call to jaws to add this path to database
#jaws insert_db save_dir
# we'll send a list ['pfam', save_dir, version]



