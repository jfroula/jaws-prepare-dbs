#!/usr/bin/env python
import pika

# make connection
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# create a queue
channel.queue_declare(queue='hello')

# send a message to queue
channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World!')
print(" [x] Sent 'Hello World!'")

# verify worked
connection.close()
